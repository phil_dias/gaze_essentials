clear all
clc

% augPerc = 0.2;

load('featstrain_Cent_c.mat');

for it = 1:size(featMap,1)
    numkey(it) = nnz(featMap(it,14:18));
end

gazeAng = atan2d(-featMap(:,end),featMap(:,end-1));

%% Augment horizontally right to left
rowsLeft_ = find(numkey' > 2 & abs(gazeAng) > 90);

rows_ = find(numkey' > 2 & gazeAng > -90 & gazeAng < 90);

augNum = length(rows_) - length(rowsLeft_);

augLeft = zeros(length(rows_),size(featMap,2));

for it = 1:length(rows_)
    row_ = rows_(it);
    feats_ = featMap(row_,:);

    % mirror nose horizontally
    feats_(:,4) = -feats_(:,4);
    
    % flip eyes
    Reye = feats_(:,6:7);
    Leye = feats_(:,8:9);
    
    feats_(:,6) = -Leye(1);
    feats_(:,7) = Leye(2);
    
    feats_(:,8) = -Reye(1);
    feats_(:,9) = Reye(2);    
    
    feats_(:,15:16) = flip(feats_(:,15:16));
    
    % flip ears
    Rear = feats_(:,10:11);
    Lear = feats_(:,12:13);
    
    feats_(:,10) = -Lear(1);
    feats_(:,11) = Lear(2);
    
    feats_(:,12) = -Rear(1);
    feats_(:,13) = Rear(2);    
    
    feats_(:,17:18) = flip(feats_(:,17:18));

    % mirror GT 
    feats_(:,end-1) = -feats_(:,end-1);

    augLeft(it,:) = feats_;

end

% randomly sample a percentage of the available augmented samples
numAvail = length(rows_);
sampledId = randperm(numAvail,augNum);

featsAug = vertcat(featMap,augLeft(sampledId,:));

gazeAng_aug = atan2d(-featsAug(:,end),featsAug(:,end-1));

%% Augment vertically
% rows_ = find(numkey >= 4);

rowsDown_ = find(numkey' > 4 & gazeAng < 0);
rowsUp_ = find(numkey' > 4 & gazeAng > 0);

augNum = length(rowsDown_) - length(rowsUp_);

% columns of each keypoint: nose, Reye, Leye, Rear, Lear
% keypts = [{'Nose'},{'Reye'},{'Leye'},{'Rear'},{'Lear'}];

augVert = zeros(length(rowsDown_),size(featMap,2));

for it = 1:length(rowsDown_)
    row_ = rowsDown_(it); 

    feats_ = featMap(row_,:);

    % discard nose
    feats_(:,4:5) = 0;
    feats_(:,14) = 0;
    % discard eyes
    feats_(:,6:9) = 0;
    feats_(:,15:16) = 0;
    % flip ears
    Rear = feats_(:,10:11);
    cRear = feats_(:,17);
    Lear = feats_(:,12:13);
    cLear = feats_(:,18);

    feats_(:,10) = Lear(1);
    feats_(:,11) = -Lear(2);
    feats_(:,12) = Rear(1);
    feats_(:,13) = -Rear(2);
    feats_(:,17) = cLear;
    feats_(:,18) = cRear;

    % rotate GT by 180
    feats_(:,end-1:end) = -feats_(:,end-1:end);
    
    augVert(it,:) = feats_;
end

% randomly sample a percentage of the available augmented samples
numAvail = length(rowsDown_);
sampledId = randperm(numAvail,floor(augNum*0.7));

featsAug = vertcat(featsAug,augVert(sampledId,:));