import os
import glob
import numpy as np
import scipy.io as sio

import cv2
import matplotlib.pyplot as plt

import keras.backend as K

# file containing NN architecture
from finalmodel import prepare_modelSingle
# file containing auxiliary functions that:
# - load poses detected from OpenPose saved as JSON
# - normalize/scale the detected facial features 
from featuresFun import load_many_poses_from_json, compute_head_features

# draw arrow illustrating gaze direction on the image
def draw_on_img(img, centr, res):
	res[0] *= img.shape[0]
	res[1] *= img.shape[1]

	norm1 = res / np.linalg.norm(res)
	norm1[0] *= img.shape[0]*0.15
	norm1[1] *= img.shape[0]*0.15


	point = centr + norm1 

	# result = cv2.circle(img, (int(point[0]),int(point[1])), 5, (0,0,255), 2)
	result = cv2.arrowedLine(img, (int(centr[0]),int(centr[1])), (int(point[0]),int(point[1])), (0,0,0), thickness=3, tipLength=0.2)
	result = cv2.arrowedLine(img, (int(centr[0]),int(centr[1])), (int(point[0]),int(point[1])), (0,0,255), thickness=2, tipLength=0.2)

	return result

# enter path where OpenPose was installed
os.chdir('/usr/local/Frameworks/openpose')

# network parameters/model
model_id = 2
activ_func = 'relu'

# file containing optimal weights of our gaze NN
model_str = 'check_model_2_opt_adam_lr_0.0005_run_0_00000686'

# FOR-loop to iterate over videos in folder
for vidId in range(9,11):
	# input folder
	root_dir = "/home/philipe/Codes/gazescripts/VillaArzilla/videos/CAM2/%d" %vidId 

	# run OpenPose on all images within root_dir
	out_json_dir = os.path.join(root_dir, 'json')
	out_rend_dir = os.path.join(root_dir, 'rendered')
	out_gaze_dir = os.path.join(root_dir, 'gaze')

	if not os.path.exists(out_gaze_dir):
		os.makedirs(out_gaze_dir)

	direc = root_dir

	direc = direc.replace(" ","\ ")
	out_rend_dir = out_rend_dir.replace(" ","\ ")
	out_json_dir = out_json_dir.replace(" ","\ ")
	out_gaze_dir = out_gaze_dir.replace(" ","\ ")	

	# define command to deploy OpenPose on the frames and run it
	cmd = "./build/examples/openpose/openpose.bin --image_dir %s --display 0 --scale_number 4 --scale_gap 0.25 --write_images %s --write_json %s --num_gpu 1 --num_gpu_start 0" \
		 % (direc, out_rend_dir, out_json_dir)
	os.system(cmd)

model = prepare_modelSingle(model_id,activ_func)
model.load_weights('%s.h5' % model_str)

# for each image:
# 1. compute features (keypoints) from OpenPose detections
# 2. deploy network for gaze estimation

for vidId in range(9,11):
	# input folder
	root_dir = "/home/philipe/Codes/gazescripts/VillaArzilla/videos/CAM2/%d" %vidId 

	# path containing JSON with OpenPose detections
	out_json_dir = os.path.join(root_dir, 'json')
	# path with rendered images (overlaid OpenPose skeleton)
	out_rend_dir = os.path.join(root_dir, 'rendered')\
	# path where images with overlaid gaze arrow will be saved
	out_gaze_dir = os.path.join(root_dir, 'gazeFT2')

	if not os.path.exists(out_gaze_dir):
		os.makedirs(out_gaze_dir)	

	out_rend_dir = out_rend_dir.replace(" ","\ ")
	out_json_dir = out_json_dir.replace(" ","\ ")
	out_gaze_dir = out_gaze_dir.replace(" ","\ ")	


	for json_filename in glob.glob(os.path.join(out_json_dir,"*.json")):

		head, suffix_  = os.path.split(json_filename)
		suffix_ = suffix_.split("_")
		suffix_ = suffix_[0]

		# collected poses from OpenPose detections
		poses, conf = load_many_poses_from_json(json_filename)

		data = []
		# for each pose
		for itP in range(0,len(poses)):
			try:
				# compute facial keypoints coordinates w.r.t. to head centroid
				features, confs, centr = compute_head_features(poses[itP], conf[itP])
				# if minimal amount of facial keypoints was detected
				if features is not None:   
					featMap = np.asarray(features)
					confMap = np.asarray(confs)

					featMap = np.reshape(featMap,(1,10))
					confMap = np.reshape(confMap,(1,5))

					centr = np.asarray(centr)
					centr = np.reshape(centr,(1,2))			

					poseFeats = np.concatenate((centr, featMap, confMap), axis=1)

					data.append(poseFeats)
			except Exception as e:
				print(e)
		
		# display on image
		imgfile = glob.glob(os.path.join(root_dir,suffix_+'*'))
		# this print is just to keep track which image/frame is currently being processed
		print(imgfile)
		img = cv2.imread(imgfile[0])
		imgRend = cv2.imread(os.path.join(out_rend_dir,suffix_ + '_rendered.png'))

		imgGaze = img
		rendGaze = imgRend		

		# if at least one valid pose (person) was detected, display his/her gaze direction on image
		if data:
			# adjust features to feed our gaze NN
			ld = np.array(data)	
			ld = np.squeeze(ld,axis=1)

			X_ = np.expand_dims(ld[:, 2:],axis=2)

			# deploy our gaze NN on these features
			pred_ = model.predict(X_, batch_size=32, verbose=0)
			
			# uncomment following line if you want to save .mat files containing features/direction of gaze
			# sio.savemat(root_dir+'/pred_%s.mat'%(suffix_),mdict={'pred_':pred_,'featsTest':ld})

			for itP in range(0,pred_.shape[0]):
				try:
					imgGaze = draw_on_img(imgGaze, ld[itP,0:2], pred_[itP,:-1])
					rendGaze = draw_on_img(rendGaze, ld[itP,0:2], pred_[itP,:-1])
				except:
					print("skipped %d" %itP)

		cv2.imwrite(os.path.join(out_gaze_dir,'gaze_' + suffix_ + '.png'),imgGaze)
		cv2.imwrite(os.path.join(out_gaze_dir,'gazeR_' + suffix_ + '.png'),rendGaze)