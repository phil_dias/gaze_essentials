close all
clear all

model = 88;
suffix_ = 'NoGate';
% suffix_ = 'CentNoAug';

% epochs = 2000;

% path to GazeFollow images
img_path = '/home/philipe/Pictures/GazeFollow/new_test/';

% path to OpenPose detections
% json_path = '/home/philipe/Google Drive/Genoa19/Damiano_OpenPose/GazeFollow/json/';
output_path = sprintf('./model%d_%s/',model,suffix_);

mkdir(output_path);
% mkdir([output_path '/test/' ]);
% mkdir(strcat(output_path,'/test/',sprintf("%.8i",0),'/'));
% mkdir(strcat(output_path,'/test/',sprintf("%.8i",1),'/'));
% mkdir(strcat(output_path,'/test/',sprintf("%.8i",2),'/'));
% mkdir(strcat(output_path,'/test/',sprintf("%.8i",3),'/'));

% load .mat with test annotations (GT)
load('/home/philipe/Pictures/GazeFollow/new_test/test2_annotations.mat');   

% load .mat with predictions and features from test set
load('/home/philipe/Codes/gazeestimation/test/featstest2.mat');
load(sprintf('./models%s/singTest2_model%d.mat',suffix_,model));

% featsTest = X_test;
% list of subfolders within OpenPose path
% subfolder_list = dir(json_path);
% subfolder_list(1:2) = [];

colors_ = lines(20);

varInfo = [];

for itFile = 1:length(test_gaze)
    file_ = test_path{itFile};
    img = imread([img_path file_]);
    
    imgDet = img;    
    
    row_ = find(featsTest(:,1) == itFile);

    if ~isempty(row_)
        unitDet_ = pred_(row_,1:2)/norm(pred_(row_,1:2));           
        
        gazeAng = atan2d(-pred_(row_,2),pred_(row_,1));          
        
        % predictions from NN
        predicted_(1) = pred_(row_,1)*size(img,2) + featsTest(row_,2);
        predicted_(2) = pred_(row_,2)*size(img,1) + featsTest(row_,3);

        % "variance" estimated by NN
        var_ = exp(pred_(row_,3));
        
        % number of keypoints that were detected by OpenPose
        numKey = nnz(featsTest(row_,[14:18]));
        
        gazeAnns_ = test_gaze{itFile};
        eyes_ = test_eyes{itFile};
        bbox_ = test_bbox{itFile};

        % convert eyes and bbox normalized coordinates to actual image
        % coordinates
        eyes_(1) = eyes_(1).*size(img,2);
        eyes_(2) = eyes_(2).*size(img,1);    
%         
        centr(1) = featsTest(row_,2);
        centr(2) = featsTest(row_,3);          

%         line = [centr(1) centr(2) predicted_(1) predicted_(2)];
%         imgDet = insertShape(imgDet,'line',line,'Color','red','LineWidth',6);          
        
        clear angError_ gtVersors angGT
        
        gazeAnns_(:,1) = gazeAnns_(:,1)*size(img,2);
        gazeAnns_(:,2) = gazeAnns_(:,2)*size(img,1);
        
        gaze_ = mean(gazeAnns_);
        
        % compute unit vector from gaze annotation 
        unitAnn_ = gaze_ - centr;
        unitAnn_ = unitAnn_/norm(unitAnn_);         
                    
        angGT = atan2d(-unitAnn_(:,2),unitAnn_(:,1));
   
        angError_ = acosd(dot(unitAnn_, unitDet_));
            
%          line = [eyes_(1) eyes_(2) gaze_(1) gaze_(2)];
%          imgDet = insertShape(imgDet,'line',line,'Color',255.*colors_(itGaze,:),'LineWidth',2);    
           
%         plot([eyes_(1),gaze_(1)],[eyes_(2),gaze_(2)],'linewidth',2);
%         gtAng = atan2d(-gazeGT_(2),gazeGT_(1));             

%         filesave_ = [output_path file_(1:end-4) '.mat'];
%         save(filesave_,'angError_','predicted_','centr');

        errorsMean(itFile) = angError_;
        
        position_ = [size(img,2) - 80,size(img,1) - 50];

%         imgDet = insertText(imgDet,position_,num2str(errorsMean(itFile)),'FontSize',14,'BoxColor',...
%         'black','BoxOpacity',0.4,'TextColor','white');    
    
        position_ = [size(img,2) - 80,size(img,1) - 30];

%         imgDet = insertText(imgDet,position_,strcat(num2str(var_),'/',num2str(numKey)),'FontSize',14,'BoxColor',...
%         'black','BoxOpacity',0.4,'TextColor','white');                            
        
        varInfo = vertcat(varInfo,[var_ numKey gazeAng angError_ angGT itFile ]);    
    else
        errorsMean(itFile) = NaN;
    end
    
    filesave_ = [output_path file_(1:end-4) '.png'];
    
%     imwrite(imgDet,filesave_,'png');

%     dists_(itFile) = minVal;
    itFile
    
end
save([output_path '/metrics_testSigB.mat'],'errorsMean','varInfo');     
mean(errorsMean,'omitnan')

%% displaying things
figure
markers_ = ['o','+','s','*','^'];
for k = 1:5
    rows_ = find(varInfo(:,2) == k);
    subplot(231),polarscatter(deg2rad(varInfo(rows_,3)),varInfo(rows_,1),[],cmap(k,:),markers_(k))
    hold on
end
% legend('k = 1','k = 2','k = 3','k = 4','k = 5');

%
% figure
markers_ = ['o','+','s','*','^'];
for k = 1:5
    rows_ = find(varInfo(:,2) == k);
    subplot(2,3,k+1),polarscatter(deg2rad(varInfo(rows_,3)),varInfo(rows_,1),[],cmap(k,:),markers_(k))
    title(['k = ' num2str(k)])
    hold on
end
% legend('k = 1','k = 2','k = 3','k = 4','k = 5');

%
markers_ = ['o','+','s','*','^'];
keypts = [{'Nose'},{'R eye'},{'L eye'},{'R ear'},{'L ear'}];
for k = 1:5   
    figure
    rows_ = find(varInfo(:,2) == k);
    subplot(231),polarscatter(deg2rad(varInfo(rows_,3)),varInfo(rows_,1),[],cmap(k,:),markers_(k));
    for j = 1:5
        keyPt_j = find(featsTest(rows_,13+j) > 0);
        
        subplot(2,3,j+1),polarscatter(deg2rad(varInfo(rows_(keyPt_j),3)),...
                        varInfo(rows_(keyPt_j),1),[],cmap(k,:),markers_(j))
        rlim([0 0.6])                    
        title([keypts{j}])                    
        hold on
    end
end

% figuresize = [0, 0, 13, 8];
% set(gcf,'units', 'centimeters');
% set(gcf,'PaperUnits','centimeters');
% set(gcf,'PaperPositionMode', 'manual');
% set(gcf,'PaperPosition', figuresize);
% set(gcf,'PaperSize',figuresize(3:4));
% set(gcf,'Position', figuresize);
% set(findall(gcf,'-property','FontSize'),'FontSize',14);
% set(findall(gcf,'-property','FontName'),'FontName','Times');
fname = 'Conf_3pts_Ang.svg';
print(fname,'-dsvg');

%%
pairs_ = nchoosek(1:5,2);
mapK = lines(size(pairs_,1));
rows_ = find(varInfo(:,2) == 2);

keypts = [{'Nose'},{'Reye'},{'Leye'},{'Rear'},{'Lear'}];

% figure,polarscatter(deg2rad(varInfo(rows_,3)),varInfo(rows_,1),[],cmap(2,:),markers_(2));
figure(1)

for itT = 1:size(pairs_,1)
    tr_ = pairs_(itT,:);
    keyPt_t = find(featsTest(rows_,13+tr_(1)) > 0 &...
                   featsTest(rows_,13+tr_(2)) > 0 ); 

    figure(1),polarscatter(deg2rad(varInfo(rows_(keyPt_t),3)),...
                        varInfo(rows_(keyPt_t),1),[],mapK(itT,:)),hold on            
               
    figure,polarscatter(deg2rad(varInfo(rows_(keyPt_t),3)),...
                        varInfo(rows_(keyPt_t),1),[],mapK(itT,:))    
    rlim([0 0.6])         
    filename_ = [keypts{tr_(1)} '_' keypts{tr_(2)} ];
    title([keypts{tr_(1)} ', ' keypts{tr_(2)}])    
    figuresize = [0, 0, 18, 10];
    set(gcf,'units', 'centimeters');
    set(gcf,'PaperUnits','centimeters');
    set(gcf,'PaperPositionMode', 'manual');
    set(gcf,'PaperPosition', figuresize);
    set(gcf,'PaperSize',figuresize(3:4));
    set(gcf,'Position', figuresize);
    set(findall(gcf,'-property','FontSize'),'FontSize',12);
    set(findall(gcf,'-property','FontName'),'FontName','Times');   
%     fname = ['./Pairs/test/' filename_ '.svg'];
%     print(fname,'-dsvg');    
    
%     close all
end
%%
triplets_ = nchoosek(1:5,3);
numColors = 0;
for itT = 1:size(triplets_,1)
    tr_ = triplets_(itT,:);
    keyPt_t = find(featsTest(rows_,13+tr_(1)) > 0 &...
                   featsTest(rows_,13+tr_(2)) > 0 &...
                   featsTest(rows_,13+tr_(3)) > 0);
    numColors = numColors + (~isempty(keyPt_t));
end

mapK = linspecer(numColors);

rows_ = find(varInfo(:,2) == 3);

keypts = [{'Nose'},{'Reye'},{'Leye'},{'Rear'},{'Lear'}];
itC = 0;
% figure
for itT = 1:size(triplets_,1)
    tr_ = triplets_(itT,:);
    keyPt_t = find(featsTest(rows_,13+tr_(1)) > 0 &...
                   featsTest(rows_,13+tr_(2)) > 0 &...
                   featsTest(rows_,13+tr_(3)) > 0); 
   
       if ~isempty(keyPt_t)               
        itC = itC+1;               
        figure(1),polarscatter(deg2rad(varInfo(rows_(keyPt_t),3)),...
                            varInfo(rows_(keyPt_t),1),[],mapK(itC,:),markers_(itT)),hold on                       

        figure(2),polarscatter(deg2rad(varInfo(rows_(keyPt_t),3)),...
                            varInfo(rows_(keyPt_t),1),[],mapK(itC,:),markers_(itT))    
        rlim([0 0.6])         
        filename_ = [keypts{tr_(1)} '_' keypts{tr_(2)} '_' keypts{tr_(3)}];
        title([keypts{tr_(1)} ', ' keypts{tr_(2)} ', ' keypts{tr_(3)}])    
        figuresize = [0, 0, 10, 10];
        set(gcf,'units', 'centimeters');
        set(gcf,'PaperUnits','centimeters');
        set(gcf,'PaperPositionMode', 'manual');
        set(gcf,'PaperPosition', figuresize);
        set(gcf,'PaperSize',figuresize(3:4));
        set(gcf,'Position', figuresize);
        set(findall(gcf,'-property','FontSize'),'FontSize',12);
        set(findall(gcf,'-property','FontName'),'FontName','Times');   
        fname = ['./Triplets/train2/' filename_ '.svg'];
%         print(fname,'-dsvg');    
    end
%     close all
end