import numpy as np
import argparse
import random
import os
import csv
import cv2
import matplotlib.pyplot as plt
import json 
import math
import scipy.io as sio
import glob

from keras.models import Sequential, Model, load_model #Sequential Models
from keras.layers import Dense, Dropout, Conv1D, LocallyConnected1D, Flatten, Input, Multiply, Lambda 
from keras.optimizers import SGD #Stochastic Gradient Descent Optimizer
from keras.optimizers import Adam 
from keras.utils import to_categorical
import keras.backend as K
import keras as keras
from keras import regularizers
from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import train_test_split
#from mymodels import prepare_modelFineTune

# from mymodels import prepare_modelSingle
from finalmodels import prepare_modelSingle
# ---------------------------------------

def angular_error(y_true,y_pred):

        y_val = K.l2_normalize(y_true[:,:-1], axis=-1)
        y_predict = K.l2_normalize(y_pred[:,:-1], axis=-1)

        dot_ = K.batch_dot(y_val,y_predict,axes=1)
        results =  K.tf.acos(K.clip(dot_, -1, 1)) / np.pi * 180

        ang_error = K.mean(results)
        print(ang_error)

        return ang_error      


# MSE with uncertainty factor
def custom_loss(y_true,y_pred):

        MSE_ = K.mean(K.square(y_pred[:,:-1] - y_true[:,:-1]), axis=-1)
        loss = MSE_/(2*K.square(y_pred[:,-1])) + K.log(K.abs(y_pred[:,-1]))

        return loss

# Cosine loss with uncertainty factor
def custom_loss2(y_true,y_pred):

        var_pred = y_pred[:,-1]
        y_true = K.l2_normalize(y_true[:,:-1], axis=-1)
        y_pred = K.l2_normalize(y_pred[:,:-1], axis=-1)
        COS_ = (-K.sum(y_true * y_pred, axis=-1)+1)/2

        loss = COS_/(2*K.square(var_pred)) + K.log(K.abs(var_pred))

        return loss

# Cosine loss with uncertainty factor and using exp to avoid division by zero
# (used in final version of experiments)
def custom_lossLog(y_true,y_pred):

        var_pred = y_pred[:,-1]
        y_true = K.l2_normalize(y_true[:,:-1], axis=-1)
        y_pred = K.l2_normalize(y_pred[:,:-1], axis=-1)
        COS_ = (-K.sum(y_true * y_pred, axis=-1)+1)/2

        loss = (COS_*K.exp(-var_pred))/2 + (var_pred/2)

        # Return a function
        return loss

def unit_vector(vector):
        """ Returns the unit vector of the vector.  """
        return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
        """ Returns the angle in radians between vectors 'v1' and 'v2'::

                        >>> angle_between((1, 0, 0), (0, 1, 0))
                        1.5707963267948966
                        >>> angle_between((1, 0, 0), (1, 0, 0))
                        0.0
                        >>> angle_between((1, 0, 0), (-1, 0, 0))
                        3.141592653589793
        """
        v1_u = unit_vector(v1)
        v2_u = unit_vector(v2)
        return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

# ---------------------------------------

def create_nn_model_exp(seed, tr_perc, load_data, modelid_par, save_par, loss_, base_path, numexp):
        
        # suffix defining where output files will be saved
        suffix_ = 'NoTrain'

        model_id = 2
        activ_func = 'relu'

        model = prepare_modelSingle(model_id, activ_func)

        for expid in np.arange(0,numexp):
                rowTable = 0

                model = prepare_modelSingle(model_id, activ_func)

                for run in np.arange(2,3):
                        setspath = base_path + 'setsplits%d/' % run                        
                        
                        setsFile = setspath + 'subSets%d.npz' % (expid)

                        data_arr = np.load(setsFile)

                        X_test = data_arr['X_test']
                        y_test = data_arr['y_test']

                        seq_test = data_arr['seq_test']

                        # pad labels with dummy column to match no of output units to handle uncertainty prediction
                        X_test = np.expand_dims(X_test,axis=2)
               
                        model.load_weights('../modelsSingleCentF/check_model_2_opt_adam_lr_0.005_decay_0_activ_relu_00000708.h5')

                        pred_ = model.predict(X_test, batch_size=32, verbose=0)

                        y_predict = np.asarray(pred_)            

                        results = [0] * y_test.shape[0]

                        for i in range(0,y_test.shape[0]):

                                results[i] = angle_between(y_test[i,:], y_predict[i,:-1]) / np.pi * 180 

                        print("\n\n\n\n###### Experiment %d:%f #######\n\n\n\n" % (expid,np.nanmean(results)))                          
                
                        model_str = 'nofinetune_exp_%d_run_%d' % (expid, run)

                        sio.savemat(base_path+'plots%s/%s.mat' % (suffix_,model_str) ,mdict={'pred_':pred_,'results':results})             
                     
        return model


# ---------------------------------------
# ADJUST PATHS ACCORDINGLY
datapath = '/home/philipe/Datasets/VillaArzilla/featsGT_Arz12.npz'
base_path = '/home/philipe/Codes/gazescripts/VillaArzilla/'
save_par = True
numexp = 7
modelid = 2
loss_ = 'sig'

clf = create_nn_model_exp(222, 1, datapath, modelid, save_par, loss_, base_path, numexp)
