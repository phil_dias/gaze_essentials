import keras.backend as K
import keras as keras

import numpy as np

from keras.models import Sequential, Model, load_model #Sequential Models
from keras.layers import Dense, Dropout, Conv1D, LocallyConnected1D, Flatten, Input, Reshape, Multiply, Concatenate, BatchNormalization, Lambda, Add, Activation
from keras.layers.advanced_activations import LeakyReLU

from keras.constraints import nonneg
from keras.initializers import Constant
from keras import regularizers

def my_init(shape, dtype=None):
    
    initVec = K.variable(np.ones(shape)*0.02)
    # initVec[:,:-1] = 
    # print(K.random_normal((shape[0],2)))
    K.set_value(initVec[:,:-1],K.eval(K.random_uniform((shape[0],2),minval=-0.1, maxval=0.1)))

    return initVec

def prepare_modelSingle(model_id,activ_func):
    if model_id == 0:       
        inputX = Input(shape=(15,1,))

        #input1 = Input(shape=(40,1));
        modelX = Sequential()
        modelX.add(Lambda(lambda x: x[:,0:10:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelX.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', activation=activ_func, bias_initializer='ones'))
        modelX.add(Flatten())

        modelY = Sequential()
        modelY.add(Lambda(lambda x: x[:,1:11:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelY.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', activation=activ_func, bias_initializer='ones'))
        modelY.add(Flatten())

        # confidence pairs
        #input2 = Input(shape=(20,1));
        modelC = Sequential()
        # modelC.add(Input(shape=(60,1)))
        modelC.add(Lambda(lambda x: 2.8532*(x[:,10:15]-0.5383),input_shape=(15,1)))
        # modelF.add(Reshape((-1,20)))
        modelC.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', kernel_regularizer=regularizers.l2(0.001), activation='sigmoid', use_bias=False))
        modelC.add(Flatten())

        modelX.build()
        # modelX.summary()

        modelC.build()
        # modelC.summary()
        # input1 = crop(0,0,40)(inputX)
        # input2 = crop(0,20,61)(inputX)

        x1 = modelX(inputX)
        y1 = modelY(inputX)
        
        c1 = modelC(inputX)

        xMult = Multiply()(([x1,c1]))
        yMult = Multiply()(([y1,c1]))

        xyMerge = Concatenate()(([xMult,yMult]))

        d0 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.001), activation=activ_func)(xyMerge)
        d1 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.001), activation=activ_func)(d0)
        out = Dense(3, kernel_initializer=my_init)(d1)
        model = Model(inputs=inputX, outputs=out)
        # model.summary()        

    elif model_id == 1:
        inputX = Input(shape=(15,1,))

        #input1 = Input(shape=(40,1));
        modelX = Sequential()
        modelX.add(Lambda(lambda x: x[:,0:10:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelX.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', activation=activ_func, bias_initializer='ones'))
        modelX.add(Flatten())

        modelY = Sequential()
        modelY.add(Lambda(lambda x: x[:,1:11:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelY.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', activation=activ_func, bias_initializer='ones'))
        modelY.add(Flatten())

        # confidence pairs
        #input2 = Input(shape=(20,1));
        modelC = Sequential()
        # modelC.add(Input(shape=(60,1)))
        modelC.add(Lambda(lambda x: 5*(x[:,10:15]-0.5),input_shape=(15,1)))
        # modelF.add(Reshape((-1,20)))
        modelC.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', kernel_regularizer=regularizers.l2(0.001), activation='sigmoid', use_bias=False))
        modelC.add(Flatten())

        modelX.build()
        # modelX.summary()

        modelC.build()
        # modelC.summary()
        # input1 = crop(0,0,40)(inputX)
        # input2 = crop(0,20,61)(inputX)

        x1 = modelX(inputX)
        y1 = modelY(inputX)
        
        c1 = modelC(inputX)

        xMult = Multiply()(([x1,c1]))
        yMult = Multiply()(([y1,c1]))

        xyMerge = Concatenate()(([xMult,yMult]))

        d0 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.001), activation=activ_func)(xyMerge)
        d1 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.001), activation=activ_func)(d0)
        out = Dense(3, kernel_initializer=my_init)(d1)
        model = Model(inputs=inputX, outputs=out)
        # model.summary()      
    
    elif model_id == 2:       
        inputX = Input(shape=(15,1,))

        #input1 = Input(shape=(40,1));
        modelX = Sequential()
        modelX.add(Lambda(lambda x: x[:,0:10:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelX.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', activation=activ_func, bias_initializer='ones'))
        modelX.add(Flatten())

        modelY = Sequential()
        modelY.add(Lambda(lambda x: x[:,1:11:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelY.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', activation=activ_func, bias_initializer='ones'))
        modelY.add(Flatten())

        # confidence pairs
        #input2 = Input(shape=(20,1));
        modelC = Sequential()
        # modelC.add(Input(shape=(60,1)))
        modelC.add(Lambda(lambda x: 2.8532*(x[:,10:15]-0.5383),input_shape=(15,1)))
        # modelF.add(Reshape((-1,20)))
        modelC.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', kernel_regularizer=regularizers.l2(0.001), activation='sigmoid', use_bias=False))
        modelC.add(Flatten())

        modelX.build()
        # modelX.summary()

        modelC.build()
        # modelC.summary()
        # input1 = crop(0,0,40)(inputX)
        # input2 = crop(0,20,61)(inputX)

        x1 = modelX(inputX)
        y1 = modelY(inputX)
        
        c1 = modelC(inputX)

        xMult = Multiply()(([x1,c1]))
        yMult = Multiply()(([y1,c1]))

        xyMerge = Concatenate()(([xMult,yMult]))

        d0 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.0001), activation=activ_func)(xyMerge)
        d1 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.0001), activation=activ_func)(d0)
        out = Dense(3, kernel_initializer=my_init)(d1)
        model = Model(inputs=inputX, outputs=out)
        # model.summary()   

    elif model_id == 3:       
        inputX = Input(shape=(15,1,))

        #input1 = Input(shape=(40,1));
        modelX = Sequential()
        modelX.add(Lambda(lambda x: x[:,0:10:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelX.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', activation=activ_func, bias_initializer='ones'))
        modelX.add(Flatten())

        modelY = Sequential()
        modelY.add(Lambda(lambda x: x[:,1:11:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelY.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', activation=activ_func, bias_initializer='ones'))
        modelY.add(Flatten())

        # confidence pairs
        #input2 = Input(shape=(20,1));
        modelC = Sequential()
        # modelC.add(Input(shape=(60,1)))
        modelC.add(Lambda(lambda x: 2.8532*(x[:,10:15]-0.5383),input_shape=(15,1)))
        # modelF.add(Reshape((-1,20)))
        modelC.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', kernel_regularizer=regularizers.l2(0.001), activation='sigmoid', use_bias=False))
        modelC.add(Flatten())

        modelX.build()
        # modelX.summary()

        modelC.build()
        # modelC.summary()
        # input1 = crop(0,0,40)(inputX)
        # input2 = crop(0,20,61)(inputX)

        x1 = modelX(inputX)
        y1 = modelY(inputX)
        
        c1 = modelC(inputX)

        xMult = Multiply()(([x1,c1]))
        yMult = Multiply()(([y1,c1]))

        xyMerge = Concatenate()(([xMult,yMult]))

        d0 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.00001), activation=activ_func)(xyMerge)
        d1 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.00001), activation=activ_func)(d0)
        out = Dense(3, kernel_initializer=my_init)(d1)
        model = Model(inputs=inputX, outputs=out)
        # model.summary()   
    
    elif model_id == 4:
        inputX = Input(shape=(15,1,))

        #input1 = Input(shape=(40,1));
        modelX = Sequential()
        modelX.add(Lambda(lambda x: x[:,0:10:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelX.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='he_normal', activation=activ_func, bias_initializer='he_normal'))
        modelX.add(Flatten())

        modelY = Sequential()
        modelY.add(Lambda(lambda x: x[:,1:11:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelY.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='he_normal', activation=activ_func, bias_initializer='he_normal'))
        modelY.add(Flatten())

        # confidence pairs
        #input2 = Input(shape=(20,1));
        modelC = Sequential()
        # modelC.add(Input(shape=(60,1)))
        modelC.add(Lambda(lambda x: 2.8532*(x[:,10:15]-0.5383),input_shape=(15,1)))
        # modelF.add(Reshape((-1,20)))
        modelC.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', kernel_regularizer=regularizers.l2(0.001), activation='sigmoid', use_bias=False))
        modelC.add(Flatten())

        modelX.build()
        # modelX.summary()

        modelC.build()
        # modelC.summary()
        # input1 = crop(0,0,40)(inputX)
        # input2 = crop(0,20,61)(inputX)

        x1 = modelX(inputX)
        y1 = modelY(inputX)
        
        c1 = modelC(inputX)

        xMult = Multiply()(([x1,c1]))
        yMult = Multiply()(([y1,c1]))

        xyMerge = Concatenate()(([xMult,yMult]))

        d0 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.0001), activation=activ_func)(xyMerge)
        d1 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.0001), activation=activ_func)(d0)
        out = Dense(3, kernel_initializer=my_init)(d1)
        model = Model(inputs=inputX, outputs=out)
        # model.summary()   
    
    elif model_id == 66:       
        inputX = Input(shape=(15,1,))

        #input1 = Input(shape=(40,1));
        modelX = Sequential()
        modelX.add(Lambda(lambda x: x[:,0:10:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelX.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', activation=activ_func, bias_initializer='ones'))
        modelX.add(Flatten())

        modelY = Sequential()
        modelY.add(Lambda(lambda x: x[:,1:11:2],input_shape=(15,1)))
        # modelF.add(Reshape((-1,40)))
        modelY.add(LocallyConnected1D(filters=1,kernel_size=(1),strides=1,kernel_initializer='ones', activation=activ_func, bias_initializer='ones'))
        modelY.add(Flatten())


        modelX.build()
        # modelX.summary()

        modelC.build()
        # modelC.summary()
        # input1 = crop(0,0,40)(inputX)
        # input2 = crop(0,20,61)(inputX)

        x1 = modelX(inputX)
        y1 = modelY(inputX)        

        xyMerge = Concatenate()(([x1,y1]))

        d0 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.0001), activation=activ_func)(xyMerge)
        d1 = Dense(10, kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(0.0001), activation=activ_func)(d0)
        out = Dense(3, kernel_initializer=my_init)(d1)
        model = Model(inputs=inputX, outputs=out)

    return model